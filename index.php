<?php

    /* Домашнее задание к лекции 1.3 «Строки, массивы и объекты»

    Жестокое обращение с животными
        Составить массив, ключами в котором будут названия континентов (на английском — Africa), значениями — массивы из латинских названий зверей (например, Mammuthus columbi – можно найти в карточке статистики о животном справа). Найдите различных зверей и составьте массив так, чтобы для ключа Africa у вас значением был бы массив из зверей, там обитавших или обитающих. Выберите только один континент для каждого животного. Пусть у вас получится 10-15 зверей.
        Теперь найдите всех зверей, название которых состоит из двух слов. Составьте из них новый массив.
        Случайно перемешайте между собой первые и вторые слова названий животных так, чтобы на выходе мы получили выдуманных, фантазийных животных. Название фантазийного животного должно начинаться с первого слова реального названия животного. Важно, чтобы каждый кусочек был использован и не повторялся более одного раза. Ничего страшного, если в результате перемешивания иногда будут получаться реальные животные. Вывести этих животных на экран.

    Дополнительно:
        Сохраните названия регионов, к которым относятся ваши звери. Принадлежность определяйте по изначальной принадлежности первого кусочка названия животного. Пусть названия регионов выводятся заголовками <h2>, а под ними – относящиеся к этому региону животные.
        Между животными выводите запятые. В конце не должно быть висящих запятых, неуместных по правилам пунктуации.
    */

    // Подготовим двумерный массив: континенты - животные
    $continents = array(
        "Africa" => array("Okapia johnstoni", "Tragelaphus eurycerus", "Hexaprotodon liberiensis", "Potamochoerus porcus", "Viverra", "Galago", "Cercopithecus", "Panthera leo", "Giraffa", "Syncerus caffer", "Hystrix", "Phacochoerus africanus", "Daubentonia madagascariensis"),
        "Antarctica" => array("Belgica antarctica", "Euphausia superba", "Aptenodytes forsteri", "Cryolophosaurus", "Pygoscelis adeliae", "Thalassoica antarctica", "Hydrurga leptonyx", "Mirounga", "Ommatophoca rossii", "Sterna paradisaea", "Stercorariidae", "Leptonychotes weddellii"),
        "Australia" => array("Notoryctes", "Spiny Anteater", "Regent Bowerbird", "Thylacinus cynocephalus", "Sarcophilus harrisii", "Dasyurus", "Ornithorhynchus anatinus", "Tachyglossus aculeatus", "Thylacinus cynocephalus", "Vombatus ursinus", "Phascolarctos cinereus", "Macropus eugenii", "Dromaius novaehollandiae"),
        "North America" => array("Ovibos moschatus", "Dicrostonyx exsul", "Lemmus amurensis", "Myopus schisticolor", "Synaptomys borealis", "Helodermatidae", "Canis latrans", "Gulo gulo", "Martes pennanti", "Martes americana", "Condylura cristata", "Phrynosoma")
    );

    // Массив для животных из двух слов
    $complexAnimals = [];

    // Найдем подходящих животных и заполним массив
    foreach($continents as $continent => $animals) {
        foreach($animals as $animal) {
            $words = explode(" ", trim($animal));
            if (count($words) == 2) {
                $complexAnimals[] = ["continent" => $continent, "name" => $animal, "firstName" => $words[0], "secondName" => $words[1]];
            }
        }
    }

    // Для выполнения задания достаточно перемешать один из массивов. Порядок первых не меняем
    $secondNames = array_column($complexAnimals, "secondName");
    shuffle($secondNames);

    // Новое название установим как дополнительное поле ассоциативного массива
    for($i = 0; $i < count($complexAnimals); $i++) {
        $animal = &$complexAnimals[$i];
        $animal["newName"] = $animal["firstName"] . " " . $secondNames[$i];
    }

    // Функция поиска животных по континентам обитания
    function findAnimalByContinent($animals, $continent, $field)
    {
        $result = [];
        foreach($animals as $animal) {
            if($animal["continent"] === $continent) {
                $result[] = $animal[$field];
            }
        }
        return $result;
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>PHP-19. Task 1.3</title>
</head>

<body>
    <?php foreach ($continents as $continent => $animals) {
        $animals = findAnimalByContinent($complexAnimals, $continent, "newName");
        if(count($animals) === 0) {
            continue;
        }

        echo "<h2>$continent</h2>";
        echo nl2br(implode(",\n", $animals));
    }?>
</body>
</html>
